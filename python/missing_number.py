r, s = int(input()), input()

b = list(map(int, s.split()))

print((r * (r + 1)) // 2 - sum(b))

