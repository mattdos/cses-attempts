n = input()
i, j = 1, 1

for _ in range(len(n) - 1):
    if n[_] is n[_ + 1]:
        i += 1
        if j < i:
            j = i
    else:
        i = 1

print(j)
