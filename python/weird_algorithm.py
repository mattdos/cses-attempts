n = int(input()); s = ''

while n != 1:
    s += f'{n} '
    if n % 2 == 0: n //= 2
    else: n = n * 3 + 1

print(s + '1')
